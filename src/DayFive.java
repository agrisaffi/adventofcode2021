import helperClasses.ReadDatFile;

import java.util.List;

public class DayFive {
    public static void main(String[] args) {
        DayFive dayFive = new DayFive();
        System.out.println("Part one answer = "+ dayFive.partOne());
        System.out.println("Part two answer = "+ dayFive.partTwo());
    }

    private int partOne() {
        List<String> ventLines = ReadDatFile.readFile("src/dataLoads/dayFive.txt");
        int[][] grid = new int[1000][1000];
        for(String v : ventLines){
            String[] startEnd = v.split("->");
            String[] start = startEnd[0].split(",");
            String[] end = startEnd[1].split(",");
            int x1 = Integer.parseInt(start[0].trim());
            int y1 = Integer.parseInt(start[1].trim());
            int x2 = Integer.parseInt(end[0].trim());
            int y2 = Integer.parseInt(end[1].trim());
            markHorzVert(grid, x1, y1, x2, y2);
        }

        int overlap =0;
        for(int p =0; p < grid.length; p++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[j][p] >= 2){
                    overlap++;
                }
            }
        }

        return overlap;
    }

    private void markDiag(int[][] grid, int x1, int y1, int x2, int y2) {
            if(x1 > x2){
                if(y1 > y2){//good
                    int i = x2;
                    int p = y2;
                    while (i != x1+1 || p != y1+1) {
                        grid[p][i] += 1;
                        i++;
                        p++;

                    }
                }
                else{
                    int i = x2;
                    int p = y2;
                    while (i != x1+1 || p != y1-1) {
                        grid[p][i] += 1;
                        i++;
                        p--;
                    }
                }
            }
            else{
                if(y1 > y2){
                    int i = x2;
                    int p = y2;
                    while (i != x1-1 || p != y1+1) {
                        grid[p][i] += 1;
                        i--;
                        p++;
                    }
                }
                else{
                    int i = x2;
                    int p = y2;
                    while (i != x1-1 || p != y1-1) {
                        grid[p][i] += 1;
                        i--;
                        p--;
                    }
                }
            }

    }

    private int partTwo() {
        List<String> ventLines = ReadDatFile.readFile("src/dataLoads/dayFive.txt");
        int[][] grid = new int[1000][1000];
        for(String v : ventLines){
            String[] startEnd = v.split("->");
            String[] start = startEnd[0].split(",");
            String[] end = startEnd[1].split(",");
            int x1 = Integer.parseInt(start[0].trim());
            int y1 = Integer.parseInt(start[1].trim());
            int x2 = Integer.parseInt(end[0].trim());
            int y2 = Integer.parseInt(end[1].trim());
            markHorzVert(grid, x1, y1, x2, y2);
            if(x1 != x2 && y1 != y2) {
                markDiag(grid, x1, y1, x2, y2);
            }
        }
        int overlap =0;
        for(int p =0; p < grid.length; p++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[j][p] >= 2){
                    overlap++;
                }
            }
        }

        return overlap;
    }

    private void markHorzVert(int[][] grid, int x1, int y1, int x2, int y2){
        if(x1 == x2){
            if(y1 > y2){
                for(int i = y2; i <=y1; i++){
                    grid[i][x1]+= 1;
                }
            }
            else{
                for(int i = y1; i<= y2; i++){
                    grid[i][x1]+= 1;
                }
            }
        }
        //horizontal
        else if(y1 == y2){
            if(x1 > x2){
                for(int i=x2; i <=x1; i++){
                    grid[y1][i]+= 1;
                }
            }
            else{
                for(int i = x1; i <= x2; i++){
                    grid[y1][i]+= 1;
                }
            }
        }
    }
}
