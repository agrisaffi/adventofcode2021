import helperClasses.ReadDatFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DayThree {
    public static void main(String[] args) {
        System.out.println("Part one answer = "+ partOne());
        System.out.println("Part two answer = "+ partTwo());
    }

    private static int partOne() {
        List<String> directions = ReadDatFile.readFile("src/dataLoads/dayThreesmall.txt");
        StringBuilder temp = new StringBuilder();

        int x = 0;
        int y = 0;
        int one = 0;
        int zero = 0;
        while (y != directions.get(0).length()) {
            if (x == directions.size() - 1) {
                if (zero > one) {
                    temp.append('0');
                } else {
                    temp.append('1');
                }
                y++;
                x = 0;
                one = 0;
                zero = 0;
            } else {
                char c = directions.get(x).charAt(y);
                if (c == '1') {
                    one++;
                } else {
                    zero++;
                }
                x++;
            }
        }
        int gamma = Integer.parseInt(temp.toString(),2);
        int epsilon = gamma ^ 4095;
        return epsilon*gamma;
    }

    private static int partTwo(){
        List<String> directions = ReadDatFile.readFile("src/dataLoads/dayThree.txt");
        List<String> dirCopy = new ArrayList<>(directions);
        int oxRating = Integer.parseInt(Objects.requireNonNull(getOxRating(directions)),2);
        int co2Rating = Integer.parseInt(Objects.requireNonNull(getCo2Ratint(dirCopy)),2);

        return oxRating*co2Rating;
    }

    private static String getCo2Ratint(List<String> dirCopy) {
        int x = 0;
        int y = 0;
        int one = 0;
        int zero = 0;
        while (y != dirCopy.get(0).length()) {
            if (x == dirCopy.size()) {
                if (zero > one) {
                    removeBitStringFromList(dirCopy,"1", y);
                } else {
                    removeBitStringFromList(dirCopy,"0", y);
                }
                y++;
                x = 0;
                one = 0;
                zero = 0;
            } else {
                char c = dirCopy.get(x).charAt(y);
                if (c == '1') {
                    one++;
                } else {
                    zero++;
                }
                x++;
            }
            if(dirCopy.size() == 1){
                return dirCopy.get(0);
            }
        }
        return null;
    }

    private static String getOxRating(List<String> directions) {
        int x = 0;
        int y = 0;
        int one = 0;
        int zero = 0;
        while (y != directions.get(0).length()) {
            if (x == directions.size()) {
                if (zero > one) {
                    removeBitStringFromList(directions,"0", y);
                } else {
                    removeBitStringFromList(directions,"1", y);
                }
                y++;
                x = 0;
                one = 0;
                zero = 0;
            } else {
                char c = directions.get(x).charAt(y);
                if (c == '1') {
                    one++;
                } else {
                    zero++;
                }
                x++;
            }
            if(directions.size() == 1){
                return directions.get(0);
            }
        }
        return null;
    }
    public static void removeBitStringFromList(List<String> bitList, String bit, int index){
        int dirInx = 0;
        while (dirInx != bitList.size()) {
            if (!Character.toString(bitList.get(dirInx).charAt(index)).equals(bit)) {
                bitList.remove(dirInx);
            } else {
                dirInx++;
            }

        }
    }
}
