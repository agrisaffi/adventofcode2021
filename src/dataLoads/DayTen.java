package dataLoads;

import helperClasses.ReadDatFile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DayTen {

    Set<Character> endStrings = new HashSet<>();

    public DayTen(){
        endStrings.add('(');
        endStrings.add('[');
        endStrings.add('{');
        endStrings.add('<');
    }

    public static void main(String[] args) {
        DayTen dayTen = new DayTen();
        System.out.println("Part one answer = "+ dayTen.partOne());
        System.out.println("Part two answer = "+ dayTen.partTwo());
    }

    private int partOne() {
        List<String> chunks = ReadDatFile.readFile("src/dataLoads/small/dayTensmall.txt");
        List<String> chunkChecker = new ArrayList<>(chunks);
        String temp[] = {"a","b"};
        String one = new String("oe");
        String two = new String("oe");

        String the = new String();
        the = one + two;

        for(String s : chunks){
            if(this.endStrings.contains(s.charAt(s.length()-1))){
                chunkChecker.remove(s);
            }
        }

        return 0;
    }
    private String partTwo() {
        return null;
    }
}


