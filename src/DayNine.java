import helperClasses.Coords;
import helperClasses.ReadDatFile;

import java.util.ArrayList;
import java.util.List;

public class DayNine {
    List<Coords> lowPointCoords = new ArrayList<>();
    public static void main(String[] args) {
        DayNine dayNine = new DayNine();
        System.out.println("Part one answer = "+ dayNine.partOne());
        System.out.println("Part two answer = "+ dayNine.partTwo());
    }

    private int partOne() {
        List<String> ventHeights = ReadDatFile.readFile("src/dataLoads/small/dayNinesmall.txt");

        int[][] heightMap = createHeightMap(ventHeights);
        List<Integer> lowPoints = findLowestPoints(heightMap);

        int total = 0;
        for(int  height: lowPoints){
            total+=height +1;
        }
        return total;

    }

    private String partTwo() {
        lowPointCoords = new ArrayList<>();
        int [][] heightMap = createHeightMap(ReadDatFile.readFile("src/dataLoads/small/dayNinesmall.txt"));
        List<Integer> lowPoints = findLowestPoints(heightMap);

        List<Integer> basins = new ArrayList<>();
        for(Coords c : lowPointCoords) {
            int basinSize = 1;
            int x = c.getxCoord();
            int y = c.getyCoord();
            int xCount = x;
            int yCount = y;

            //move left
            int prev = heightMap[x][y];
            while (true) {
                yCount--;
                if (yCount >= 0) {
                    if (prev < heightMap[x][yCount] && heightMap[x][yCount] != 9) {
                        prev = heightMap[x][yCount];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            yCount = y;
            prev = heightMap[x][y];
            //move down left
            while (true) {
                yCount--;
                xCount++;
                if (yCount >= 0 && xCount < heightMap.length) {
                    if (prev < heightMap[xCount][yCount] && heightMap[xCount][yCount] != 9) {
                        prev = heightMap[xCount][yCount];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            yCount = y;
            xCount = x;
            prev = heightMap[x][y];
            //move down
            while (true) {
                xCount++;
                if (xCount < heightMap.length) {
                    if (prev < heightMap[xCount][y] && heightMap[xCount][y] != 9) {
                        prev = heightMap[xCount][y];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            xCount = x;
            prev = heightMap[x][y];
            //move right down
            while (true) {
                yCount++;
                xCount++;
                if (xCount < heightMap.length && yCount < heightMap[0].length) {
                    if (prev < heightMap[xCount][yCount] && heightMap[xCount][yCount] != 9) {
                        prev = heightMap[xCount][yCount];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            xCount = x;
            yCount = y;
            prev = heightMap[x][y];
            //move right
            while (true) {
                yCount++;
                if (yCount < heightMap[0].length) {
                    if (prev < heightMap[x][yCount] && heightMap[x][yCount] != 9) {
                        prev = heightMap[x][yCount];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }

            yCount = y;
            prev = heightMap[x][y];
            //move right up
            while (true) {
                xCount--;
                yCount++;
                if (xCount >= 0 && yCount < heightMap[0].length) {
                    if (prev < heightMap[xCount][yCount] && heightMap[xCount][yCount] != 9) {
                        prev = heightMap[xCount][yCount];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }

            xCount = x;
            yCount = y;
            prev = heightMap[x][y];
            //move up
            while (true) {
                xCount--;
                if (xCount >= 0) {
                    if (prev < heightMap[xCount][y] && heightMap[xCount][y] != 9) {
                        prev = heightMap[xCount][y];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }

            xCount = x;
            prev = heightMap[x][y];

            //move up left
            while (true) {
                xCount--;
                yCount--;
                if (xCount >= 0 && yCount >= 0) {
                    if (prev < heightMap[xCount][y] && heightMap[xCount][y] != 9) {
                        prev = heightMap[xCount][y];
                        basinSize++;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            basins.add(basinSize);
        }



        return null;
    }

    private int[][] createHeightMap(List<String> data){
        int[][] heightMap = new int[data.size()][10];//data.size()];
        int x = -1;
        int y = -1;
        for(String s : data){
            x++;
            for(char c : s.toCharArray()){
                y++;
                heightMap[x][y] = Character.getNumericValue(c);
            }
            y= -1;
        }
        return heightMap;
    }

    private List<Integer> findLowestPoints(int[][] heightMap){
        List<Integer> lowPoints = new ArrayList<>();
        for(int i = 0; i < heightMap.length; i++){
            for(int j = 0; j < heightMap[0].length; j++){
                if(i == 0) {
                    if (j == 0) {//top left corner
                        if (heightMap[i][j + 1] > heightMap[i][j] && heightMap[i + 1][j] > heightMap[i][j]) {
                            lowPoints.add(heightMap[i][j]);
                            Coords temp = new Coords(i, j);
                            lowPointCoords.add(temp);
                        }
                    }
                    else if(j == heightMap[0].length -1){ //bottom left corner
                        if(heightMap[i][j-1] > heightMap[i][j] && heightMap[i+1][j] > heightMap[i][j]){
                            lowPoints.add(heightMap[i][j]);
                            Coords temp = new Coords(i, j);
                            lowPointCoords.add(temp);
                        }
                    } else{//left side
                        if (heightMap[i][j-1] > heightMap[i][j]
                                && heightMap[i + 1][j] > heightMap[i][j]
                                && heightMap[i][j+1] > heightMap[i][j]){
                            lowPoints.add(heightMap[i][j]);
                            Coords temp = new Coords(i, j);
                            lowPointCoords.add(temp);
                        }
                    }
                }
                else if(i == heightMap.length -1){
                    if (j == 0) {//top right corner
                        if (heightMap[i][j + 1] > heightMap[i][j] && heightMap[i - 1][j] > heightMap[i][j]) {
                            lowPoints.add(heightMap[i][j]);
                            Coords temp = new Coords(i, j);
                            lowPointCoords.add(temp);
                        }
                    }
                    else if(j == heightMap[0].length -1){ //bottom right corner
                        if(heightMap[i][j-1] > heightMap[i][j] && heightMap[i-1][j] > heightMap[i][j]){
                            lowPoints.add(heightMap[i][j]);
                            Coords temp = new Coords(i, j);
                            lowPointCoords.add(temp);
                        }
                    } else{//right side
                        if (heightMap[i][j-1] > heightMap[i][j]
                                && heightMap[i - 1][j] > heightMap[i][j]
                                && heightMap[i][j+1] > heightMap[i][j]){
                            lowPoints.add(heightMap[i][j]);
                            Coords temp = new Coords(i, j);
                            lowPointCoords.add(temp);
                        }
                    }
                }
                else if(j == 0){//top
                    if(heightMap[i-1][j] > heightMap[i][j]
                            && heightMap[i][j+1]> heightMap[i][j]
                            && heightMap[i+1][j] > heightMap[i][j]){
                        lowPoints.add(heightMap[i][j]);
                        Coords temp = new Coords(i, j);
                        lowPointCoords.add(temp);
                    }
                }
                else if(j == heightMap[0].length -1){//bottom
                    if(heightMap[i-1][j] > heightMap[i][j]
                            && heightMap[i][j-1]> heightMap[i][j]
                            && heightMap[i+1][j] > heightMap[i][j]){
                        lowPoints.add(heightMap[i][j]);
                        Coords temp = new Coords(i, j);
                        lowPointCoords.add(temp);
                    }
                }
                else {//in middle
                    if(heightMap[i-1][j] > heightMap[i][j]
                            && heightMap[i][j-1]> heightMap[i][j]
                            && heightMap[i+1][j] > heightMap[i][j]
                            && heightMap[i][j+1] > heightMap[i][j]){
                        lowPoints.add(heightMap[i][j]);
                        Coords temp = new Coords(i, j);
                        lowPointCoords.add(temp);
                    }
                }
            }
        }
        return lowPoints;
    }
}
