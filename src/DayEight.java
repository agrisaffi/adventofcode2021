import helperClasses.Digits;
import helperClasses.ReadDatFile;

import java.util.List;
import java.util.stream.Collectors;

public class DayEight {
    public static void main(String[] args) {
        DayEight dayEight = new DayEight();
        System.out.println("Part one answer = "+ dayEight.partOne());
        System.out.println("Part two answer = "+ dayEight.partTwo());
    }

    private int partOne() {
        Digits digits = new Digits(false);
        List<String> signalPatterns = ReadDatFile.readFile("src/dataLoads/dayEight.txt")
                .stream().map(s -> s.split("\\|")[1]).collect(Collectors.toList());
        int digitCount = 0;
        for(String s : signalPatterns){
            String [] signals = s.trim().split(" ");
            for(String sig : signals){
                if(sig.length() == digits.getEightSet().size()){
                    digitCount++;
                }
                else if(sig.length() == digits.getSevenSet().size()){
                    digitCount++;
                }
                else if(sig.length() == digits.getFourSet().size()){
                    digitCount++;
                }
                else if(sig.length() == digits.getOneSet().size()){
                    digitCount++;
                }
            }
        }
        return digitCount;
    }

    private int partTwo() {
        Digits digits = new Digits(true);
        List<String> signalPatterns = ReadDatFile.readFile("src/dataLoads/dayEight.txt");
        int total = 0;
        for (String s : signalPatterns) {
            //each of these strings represents each digit 0-9.
            String[] sigToOutput = s.trim().split("\\|");
            String[] signals = sigToOutput[0].trim().split(" ");
            String[] output = sigToOutput[1].trim().split(" ");
            for (String sig : signals) {
                if (sig.length() == 7) {
                    digits.setEight(sig);
                } else if (sig.length() == 3) {
                    digits.setSeven(sig);
                } else if (sig.length() == 4) {
                    digits.setFour(sig);
                } else if (sig.length() == 2) {
                    digits.setOne(sig);
                }
            }
            for(String sig : signals) {
                if(sig.length() == 6){
                 //0, 6,9
                    boolean noOneFlag = false;
                    boolean noFourFlag = false;
                    char[] oneArray = digits.getOne().toCharArray();
                    for (char c : oneArray){
                        if(!sig.contains(String.valueOf(c))){
                            noOneFlag = true;
                            break;
                        }
                    }
                    if(noOneFlag){
                        digits.setSix(sig);//6
                        continue;
                    }
                    char[] fourArray = digits.getFour().toCharArray();
                    for(char c : fourArray){
                        if(!sig.contains(String.valueOf(c))){
                            noFourFlag = true;
                            break;
                        }
                    }
                    if(noFourFlag){
                        digits.setZero(sig);
                    } else{
                        digits.setNine(sig);
                    }
                }
                else if(sig.length() == 5){
                    //2,3,5
                    boolean noOneFlag = false;
                    char[] oneArray = digits.getOne().toCharArray();
                    for(char c : oneArray){
                        if(!sig.contains(String.valueOf(c))){
                            noOneFlag = true;
                            break;
                        }
                    }
                    if(!noOneFlag){
                        digits.setThree(sig);
                    }
                    else{
                        int fourCount = 0;
                        String four = digits.getFour();
                        List<Character> chars = four
                                .chars()
                                .mapToObj(e -> (char)e).collect(Collectors.toList());
                        for(char c : sig.toCharArray()){
                            if(chars.contains(c)){
                                fourCount++;
                            }
                        }
                        if(fourCount == 3){
                            digits.setFive(sig);
                        }
                        else{
                            digits.setTwo(sig);
                        }
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            for (String out : output){
                if(out.length() == 7) {
                   sb.append("8");
                } else if (out.length() == 3) {
                    sb.append("7");
                } else if (out.length() == 4) {
                    sb.append("4");
                } else if (out.length() == 2) {
                    sb.append("1");
                }
                else{
                    if(out.length() == 6){ //0,6,9
                        char[] letters = out.toCharArray();
                        boolean notZero = false;
                        boolean notSix = false;
                        boolean notNine = false;
                        for(char c : letters) {
                            if (!digits.getZero().contains(String.valueOf(c))) {
                                notZero = true;
                            }
                            if(!digits.getSix().contains(String.valueOf(c))){
                                notSix = true;
                            }
                            if(!digits.getNine().contains(String.valueOf(c))){
                                notNine = true;
                            }
                        }
                        if(notSix && notZero){
                            sb.append("9");
                        }
                        if(notSix && notNine){
                            sb.append("0");
                        }
                        if(notZero && notNine){
                            sb.append("6");
                        }
                    }
                    else{
                        char[] letters = out.toCharArray();
                        boolean notThree = false;
                        boolean notFive = false;
                        boolean notTwo = false;
                        for(char c :letters){
                            if(!digits.getThree().contains(String.valueOf(c))){
                                notThree = true;
                            }
                            if(!digits.getFive().contains(String.valueOf(c))){
                                notFive = true;
                            }
                            if(!digits.getTwo().contains(String.valueOf(c))){
                                notTwo = true;
                            }
                        }
                        if(notTwo && notThree){
                            sb.append("5");
                        }
                        if(notTwo && notFive){
                            sb.append("3");
                        }
                        if(notThree && notFive){
                            sb.append("2");
                        }
                    }
                }
            }
            int value = Integer.parseInt(sb.toString());
            total += value;
        }
        return total;
    }
}
