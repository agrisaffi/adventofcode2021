import helperClasses.ReadDatFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaySeven {
    public static void main(String[] args) {
        DaySeven daySeven = new DaySeven();
        System.out.println("Part one answer = "+ daySeven.partOne());
        System.out.println("Part two answer = "+ daySeven.partTwo());
    }

    private int partOne() {
        List<String> crabSteps = ReadDatFile.readFile("src/dataLoads/daySeven.txt");
        List<String> copyCrabSteps = new ArrayList<>(crabSteps);
        Map<String, Integer> lineUpToSteps = new HashMap<>();
        int steps = 0;
        for(String s : crabSteps){
            int target = Integer.parseInt(s);
            for(String c : copyCrabSteps){
                int startingPoint = Integer.parseInt(c);
                if(startingPoint > target) {
                    for (int i = startingPoint; i > target; i--) {
                        steps++;
                    }
                }
                else{
                    for (int i = startingPoint; i < target; i++) {
                        steps++;
                    }
                }
            }
            lineUpToSteps.put(s, steps);
            steps = 0;
        }

        int lowest = Integer.MAX_VALUE;
        for(String key : lineUpToSteps.keySet()){
            int temp = lineUpToSteps.get(key);
            if (temp < lowest){
                lowest = temp;
            }
        }
        return lowest;
    }

    private int partTwo() {
        List<String> crabSteps = ReadDatFile.readFile("src/dataLoads/daySeven.txt");
        List<String> copyCrabSteps = new ArrayList<>(crabSteps);
        int steps = 0;
        Map<String, Integer> lineUpToSteps = new HashMap<>();
        for(String s : crabSteps){
            int target = Integer.parseInt(s);
            for(String c : copyCrabSteps){
                int startingPoint = Integer.parseInt(c);
                int fuel = 1;
                if(startingPoint > target) {
                    for (int i = startingPoint; i > target; i--) {
                        steps+=fuel;
                        fuel++;
                    }
                }
                else{
                    for (int i = startingPoint; i < target; i++) {
                        steps+=fuel;
                        fuel++;
                    }
                }
            }
            lineUpToSteps.put(s, steps);
            steps = 0;
        }
        int lowest = Integer.MAX_VALUE;
        for(String key : lineUpToSteps.keySet()){
            int temp = lineUpToSteps.get(key);
            if (temp < lowest){
                lowest = temp;
            }
        }
        return lowest;
    }



}
