import helperClasses.ReadDatFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import helperClasses.ReadDatFile;

public class DayOne {

    public static void main(String[] args) {
        System.out.println("Part one answer = "+ partOne());
        System.out.println("Part two answer = "+ partTwo());
    }

    private static String partOne() {
        List<Integer> depths = ReadDatFile.readFile("src/dataLoads/dayOne.txt")
                .stream().map(Integer::parseInt).collect(Collectors.toList());
        return getIncreaseNum(depths);
    }

    /**
     * Method that iterates over all depths and compares it to the current depth.
     * If it's greater, then the currDepth is updated and we add one to the numIncrease.
     *
     * @param depths list of depths
     * @return number of times the depth increases
     */
    private static String getIncreaseNum(List<Integer> depths){
        int currDepth = depths.get(0);
        int numIncreased = 0;
        for(int i : depths){
            if(i > currDepth){
                numIncreased++;
            }
            currDepth = i;
        }
        return String.valueOf(numIncreased);
    }

    /**
     * Iterates over the depths and adds every group of three. Then passes
     * That list to the getIncreaseNum method.
     *
     * @return number of times the depth increases
     */
    private static String partTwo(){
        List<Integer> depths = ReadDatFile.readFile("src/dataLoads/dayOne.txt")
                .stream().map(Integer::parseInt).collect(Collectors.toList());

        List<Integer> groupTotals = new ArrayList<>();
        int sum = 0;
        int counter = 0;
        for(int i =0; i < depths.size(); i++){
            if(counter == 3){
                groupTotals.add(sum);
                sum = 0;
                counter = 0;
                i = i-2;
            }
            sum += depths.get(i);
            counter++;
        }
        if(sum != 0){
            groupTotals.add(sum);
        }
        return getIncreaseNum(groupTotals);
    }
}
