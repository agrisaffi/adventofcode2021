import helperClasses.ReadDatFile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DaySix {
    public static void main(String[] args) {
        DaySix daySix = new DaySix();
        System.out.println("Part one answer = "+ daySix.partOne());
        System.out.println("Part two answer = "+ daySix.partTwo());
    }

    private int partOne() {
        List<String> fish = ReadDatFile.readFile("src/dataLoads/daySix.txt");
        List<Integer> fishLives = Arrays.stream(fish.get(0).split(",")).map(Integer::parseInt).collect(Collectors.toList());

        int day = 0;
        boolean addFish = false;
        int numAddFish = 1;
        while(day != 80){
            for(int i = 0; i < fishLives.size(); i++){
                if(fishLives.get(i) == 0){
                    if(addFish){
                        numAddFish++;
                    }
                    addFish = true;
                    fishLives.set(i, 6);
                }
                else {
                    int val = fishLives.get(i) - 1;
                    fishLives.set(i, val);
                }

            }
            if(addFish){
                int i = 1;
                while( i <= numAddFish){
                    fishLives.add(8);
                    i++;
                }
                addFish=false;
                numAddFish =1;
            }
            day++;
        }
        return fishLives.size();
    }

    private long partTwo() {
        List<String> fish = ReadDatFile.readFile("src/dataLoads/daySix.txt");
        long[] numOfFishArray = new long[10];
        long total= fish.size();

        for(String s:fish){
            int i  = Integer.parseInt(s);
            numOfFishArray[i]++;
        }

        int day = 0;
        while(day != 256) {
            for (int j =0; j <= 9; j++) {
                switch (j) {
                    case 0:
                        numOfFishArray[7] += numOfFishArray[j];
                        numOfFishArray[9] += numOfFishArray[j];
                        total+= numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 1:
                        numOfFishArray[0] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 2:
                        numOfFishArray[1] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 3:
                        numOfFishArray[2] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 4:
                        numOfFishArray[3] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 5:
                        numOfFishArray[4] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 6:
                        numOfFishArray[5] += numOfFishArray[j];
                        numOfFishArray[j] =0;
                        break;
                    case 7:
                        numOfFishArray[6] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 8:
                        numOfFishArray[7] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                        break;
                    case 9:
                        numOfFishArray[8] += numOfFishArray[j];
                        numOfFishArray[j] = 0;
                }
            }
            day++;
        }
        //128537496359 too low
        //1856479209887307978 too high
        return total;
    }


}
