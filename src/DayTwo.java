import helperClasses.ReadDatFile;

import java.util.List;

public class DayTwo {
    public static void main(String[] args) {
        System.out.println("Part one answer = "+ partOne());
        System.out.println("Part two answer = "+ partTwo());
    }

    private static int partOne() {
        List<String> directions = ReadDatFile.readFile("src/dataLoads/dayTwo.txt");
        int horz = 0;
        int depth = 0;
        for(String s: directions){
            String[] splitDir = s.split(" ");
            String dir = splitDir[0];
            int steps = Integer.parseInt(splitDir[1]);
            switch (dir) {
                case "forward" :
                    horz += steps;
                case "up":
                    depth -= steps;
                case "down":
                    depth += steps;
            }
        }
        return horz*depth;
    }


    private static int partTwo() {
        List<String> directions = ReadDatFile.readFile("src/dataLoads/dayTwo.txt");
        int horz = 0;
        int depth = 0;
        int aim = 0;
        for(String s: directions){
            String[] splitDir = s.split(" ");
            String dir = splitDir[0];
            int steps = Integer.parseInt(splitDir[1]);
            switch (dir) {
                case "forward":
                    horz += steps;
                    depth += (aim * steps);

                case "up":
                    aim -= steps;
                case "down" :
                    aim += steps;
            }
        }
        return horz*depth;
    }
}
