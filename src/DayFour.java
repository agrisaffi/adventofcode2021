import helperClasses.BingoBoards;
import helperClasses.ReadDatFile;

import java.util.List;

public class DayFour {


    public static void main(String[] args) {
        DayFour dayFour = new DayFour();
        System.out.println("Part one answer = "+ dayFour.partOne());
        System.out.println("Part two answer = "+ dayFour.partTwo());
    }

    private int partOne() {
        List<String> bingoBoardInfo = ReadDatFile.readFile("src/dataLoads/dayFour.txt");
        String drawnNums = bingoBoardInfo.remove(0);
        bingoBoardInfo.remove(0); //remove whitespace
        BingoBoards bingoBoard = new BingoBoards();
        bingoBoard.createBingoBoards(bingoBoardInfo);
        for(String s: drawnNums.split(",")){
            bingoBoard.markNumberOnGrid(s);
            int val = bingoBoard.checkForBingo();
            if(val != 0){
                int sVal = Integer.parseInt(s);
                return val * sVal;
            }
        }

    return 0;
    }

    private int partTwo(){
        List<String> bingoBoardInfo = ReadDatFile.readFile("src/dataLoads/dayFour.txt");
        String drawnNums = bingoBoardInfo.remove(0);
        bingoBoardInfo.remove(0); //remove whitespace
        BingoBoards bingoBoard = new BingoBoards();
        bingoBoard.createBingoBoards(bingoBoardInfo);
        for(String s: drawnNums.split(",")){
            bingoBoard.markNumberOnGrid(s);
            int val = bingoBoard.checkForLastBingo();
            if(val != 0){
                int sVal = Integer.parseInt(s);
                return val * sVal;
            }
        }
        return 0;
    }


}
