package helperClasses;

import java.util.HashSet;
import java.util.Set;

public class Digits {
    Set<Character> oneSet = new HashSet<>();
    Set<Character> fourSet = new HashSet<>();
    Set<Character> sevenSet = new HashSet<>();
    Set<Character> eightSet = new HashSet<>();
    String zero;
    String one;
    String two;
    String three;
    String four;
    String five;
    String six;
    String seven;
    String eight;
    String nine;

    /*
        0
      1   2
        3
      4   5
        6
     */
    char[] letterOrder = new char[7];

    public Digits(boolean empty){
        if(!empty) {
            oneSet.add('c');
            oneSet.add('f');

            fourSet.add('b');
            fourSet.add('c');
            fourSet.add('d');
            fourSet.add('f');

            sevenSet.add('a');
            sevenSet.add('c');
            sevenSet.add('f');

            eightSet.add('a');
            eightSet.add('b');
            eightSet.add('c');
            eightSet.add('d');
            eightSet.add('e');
            eightSet.add('f');
            eightSet.add('g');
        }
    }


    public Set<Character> getOneSet() {
        return oneSet;
    }

    public Set<Character> getFourSet() {
        return fourSet;
    }

    public Set<Character> getSevenSet() {
        return sevenSet;
    }

    public Set<Character> getEightSet() {
        return eightSet;
    }

    public String getZero() {
        return zero;
    }

    public String getOne() {
        return one;
    }

    public String getTwo() {
        return two;
    }

    public String getThree() {
        return three;
    }

    public String getFour() {
        return four;
    }

    public String getFive() {
        return five;
    }

    public String getSix() {
        return six;
    }

    public String getSeven() {
        return seven;
    }

    public String getEight() {
        return eight;
    }

    public String getNine() {
        return nine;
    }

    public void setZero(String zero) {
        this.zero = zero;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public void setThree(String three) {
        this.three = three;
    }

    public void setFour(String four) {
        this.four = four;
    }

    public void setFive(String five) {
        this.five = five;
    }

    public void setSix(String six) {
        this.six = six;
    }

    public void setSeven(String seven) {
        this.seven = seven;
    }

    public void setEight(String eight) {
        this.eight = eight;
    }

    public void setNine(String nine) {
        this.nine = nine;
    }
}
