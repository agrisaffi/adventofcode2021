package helperClasses;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class that holds all bingo boards and a SET of deleted ones.
 */
public class BingoBoards {
    List<BingoBoard> boards = new ArrayList<>();
    Set<Integer> deleted = new HashSet<>();


    /**
     * Create a BingoBoard object from each set of 5 Strings
     *
     * @param bingoBoardInfo list of Strings that holds all bingo board data
     */
    public void createBingoBoards(List<String> bingoBoardInfo) {
        List<String> temp = new ArrayList<>();
        for (String s : bingoBoardInfo) {
            if(s.equals("")){
                continue;
            }
            temp.add(s);
            if (temp.size() == 5) {
                BingoBoard bingoBoard = new BingoBoard(temp);
                temp.clear();
                boards.add(bingoBoard);
            }
        }
    }

    /**
     * Mark the grid with an "X" if it matches s
     *
     * @param s the number called
     */
    public void markNumberOnGrid(String s) {
        for (BingoBoard b :boards) {
            String[][] grid = b.boardGrid;
            for(int i = 0; i < grid.length; i ++ ){
                for(int j = 0; j < grid[0].length; j++){
                    if(grid[i][j].equals(s)){
                        grid[i][j] = "X";
                    }
                }
            }
        }

    }

    /**
     * Iterate through the board and look for 5 "X"'s indicating a bingo. If a bingo is found.
     * Add all unmarked numbers.
     *
     * @return sum of all unmarked numbers
     */
    public int checkForBingo() {
        for (BingoBoard b :boards) {
            String[][] grid = b.boardGrid;
            int xCount = 0;
            //check horizontal
            for (String[] strings : grid) {
                for (int j = 0; j < grid[0].length; j++) {
                    if (strings[j].equals("X")) {
                        xCount++;
                    }
                    if (xCount == 5) {
                        return calcSum(grid);
                    }
                }
                xCount = 0;
            }
            int yCount = 0;
            for(int i = 0; i < grid.length; i ++ ){
                for(int j = 0; j < grid[0].length; j++){
                    if(grid[j][i].equals("X")){
                        yCount++;
                    }
                    if(yCount == 5){
                        return calcSum(grid);
                    }
                }
                yCount = 0;
            }
        }
        return 0;
    }

    /**
     * Add all unmarked numbers
     *
     * @param grid bingo board that has a bingo
     * @return the sum of all unmarked numbers
     */
    private int calcSum(String[][] grid) {
        int sum = 0;
        for (String[] strings : grid) {
            for (int j = 0; j < grid[0].length; j++) {
                if (!strings[j].equals("X")) {
                    sum += Integer.parseInt(strings[j]);
                }
            }
        }
        return sum;
    }

    /**
     * Checks for a Bingo and then adds the index to the boards list to the deleted Set. If the Set
     * size equals the board size, I know that I'm on the last grid. Then calculate the sum of the unmarked
     * squares on the last grid.
     *
     * @return the sum of all unmarked numbers on the last bingo card.
     */
    public int checkForLastBingo() {
        for(int p = 0; p < boards.size(); p ++){
            String[][] grid = boards.get(p).boardGrid;
            int xCount = 0;
            //check horizontal
            for (String[] strings : grid) {
                for (int j = 0; j < grid[0].length; j++) {
                    if (strings[j].equals("X")) {
                        xCount++;
                    }
                    if (xCount == 5) {
                        deleted.add(p);
                        if (deleted.size() == boards.size()) {
                            return calcSum(grid);
                        }
                        break;
                    }
                }
                xCount = 0;
            }
            int yCount = 0;
            for(int i = 0; i < grid.length; i ++ ){
                for(int j = 0; j < grid[0].length; j++){
                    if(grid[j][i].equals("X")){
                        yCount++;
                    }
                    if(yCount == 5){
                        deleted.add(p);
                        if(deleted.size() == boards.size()){
                            return calcSum(grid);
                        }
                    }
                }
                yCount = 0;
            }
        }

        return 0;
    }

    /**
     * Class for a Bingo board. This consits of a multidimensional array.
     */
    public static class BingoBoard{
        String[][] boardGrid = new String[5][5];

        public BingoBoard(List<String> board){
            int row = -1;
            int column = 0;
            for(String s : board){
                row++;
                for(String split :  s.split(" ")){
                    if(split.equals("")){
                        continue;
                    }
                    boardGrid[row][column] = split;
                    column++;
                }
                column = 0;
            }
        }
    }
}
